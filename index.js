const express = require('express');
require('dotenv').config();
// const db = require('./db_connection');

var bodyParser = require('body-parser');
const router = require('./Router');
const app = express();
const {PORT} = process.env;
var cors = require('cors');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
// server your css as static
app.use(express.static(__dirname));
app.use(cors());
app.get('/',(req,res)=>{
     res.send("API working");
})

app.use('/api',router);
app.listen(PORT,()=>{console.log(`server run on ${PORT}`)});